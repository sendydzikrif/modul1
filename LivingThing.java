public abstract class LivingThing {
	public void breath() {
		System.out.println("Living Thing breathing. . ."); 
	}
	
	public void eat() {
		System.out.println("Living Thing eating. . ."); 
	}

	/**
	 * abstract method walk
	 * kita ingin method ini di-overriden oleh superclass
	*/

	public abstract void walk ();
}





