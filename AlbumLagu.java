class AlbumLagu {
	String judul;
	String artis;
	int tahunrilis;
	// konstruktor
	public AlbumLagu(String judul, String artis, int tahunrilis) {
		this.judul = judul;
		this.artis = artis;
		this.tahunrilis = tahunrilis;
	}

	public String infoJudul () {
		return(judul);
	}

	public String infoArtis () {
		return(artis);
	}

	public int infoTahunRilis () {
		return(tahunrilis);
	}

	public static void main (String args [] ) {

		AlbumLagu album = new AlbumLagu ("I Love Java Code ","Rzk",2016);
		System.out.println("Judul Album : " + album.infoJudul ());
		System.out.println("Artis : " + album.infoArtis ());
		System.out.println("Tahun : " + album.infoTahunRilis ());
	}
}


