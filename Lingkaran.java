/**
* Kelas Lingkaran
* kelas ini merepresentasikan Lingkaran sebagai suatu tipe data,
* sebuah bangun datar sudut,
* berupa himpunan titik-titik yang berjarak sama ke sebuah titik pusat
* di bawah field yang dimilikinya adalah jejari, yang menyatakan jarak
* titik-titik itu ke titik pusat
*/

public class Lingkaran {
	float jejari;
	Lingkaran(){
		jejari = 0;
	}

	Lingkaran(float r){
		jejari = r;
	}
}