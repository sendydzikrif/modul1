import java.awt.Color;
public class LingkunganRumah {
	public static void main (String args [] ) {
		System.out.println("=========Michael=========");
		Kucing michael = new Kucing ();
		michael.warnaBulu = new Color (0 , 1 , 1);
		michael.nama = "Michael" ;
		michael.usia = 3 ;
		michael.bb = 4.5;
		michael.diadopsi ("Rezki");
		michael.cetakInformasi();
		System.out.println(" ");
		
		System.out.println("=========Garfield=========");
		Kucing garfield = new Kucing ();
		garfield.warnaBulu = new Color (1 , 1 , 1);
		garfield.nama = "Garfield" ;
		garfield.usia = 5 ;
		garfield.bb = 5.1;
		garfield.diadopsi ("Rahmah");
		garfield.cetakInformasi();
	}
}