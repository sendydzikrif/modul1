public class Human extends LivingThing{
	public void walk(){
		System.out.println("Human walks...");
	}

	public static void main (String[] args) {
		Human Daniel = new Human();
		Daniel.breath();
		Daniel.eat();
		Daniel.walk();
		
	}
}
