public class Circle extends Shape {
	public String color;

	public String getColor(){
		return color;
	}

	public void setColor(String color){
		this.color = color;
	}

	

	public static void main (String[] args) {
		Circle bola = new Circle();

		System.out.println(" Nama : ");
		bola.setName("Bola Basket");
		System.out.println(bola.getName());

		System.out.println("------------------------------");

		System.out.println(" Area : ");
		bola.setArea("140cm");
		System.out.println(bola.getArea());

		System.out.println("------------------------------");

		System.out.println(" Warna : ");
		bola.setColor("Orange");
		System.out.println(bola.getColor());

		System.out.println("------------------------------");
		
		
	}

}