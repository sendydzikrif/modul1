public abstract class Shape {
	public String area;
	public String name;
	public String color;

	public String getArea(){
		return area;
	}

	public void setArea(String area){
		this.area = area;
	}

	public String getName(){
		return name;
	}

	public void setName(String name){
		this.name = name;
	}

	public abstract void setColor(String color);
	public abstract String getColor();

	
}





