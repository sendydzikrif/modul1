public class Employe extends Person2 {

	public String getName() {
		System.out.println("Employee Name : " +name);
		return name;
	}

	public static void main (String[] args) {
		Person2 ref;
		Student1 studentObject = new Student1 ();
		Employe employeObject = new Employe ();

		ref = studentObject; // person menunjuk kepada object student

		String temp = ref.getName(); //getName dari student class dipanggil
		System.out.println(temp);

		ref = employeObject; //person menunjuk kepada object employe

		temp = ref.getName(); //getName dari employe class dipanggil
		System.out.println(temp);
	}
}


