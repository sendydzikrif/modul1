public class Zoo {
	public static void main (String args [] ) {
		System.out.println("=========Lion========");
		Lion Singa = new Lion ();
		Singa.nama = "Raja Hutan" ;
		Singa.usia = 10 ;
		Singa.bb = 21;
		Singa.AlamLiar ("Alam Liar");
		Singa.cetakInformasi();
		System.out.println("");		

		System.out.println("========Horse=======");
		Horse James = new Horse ();
		James.nama = "Michael" ;
		James.usia = 7 ;
		James.bb = 70;
		James.diadopsi ("Amah");
		James.cetakInformasi();
		System.out.println("");

		System.out.println("=======Kangoroo======");
		Kangoroo Bond = new Kangoroo ();
		Bond.nama = "Bond" ;
		Bond.usia = 5 ;
		Bond.bb = 45;
		Bond.TempatAsal ("Australia");
		Bond.cetakInformasi();
		System.out.println("");
		
	}
}