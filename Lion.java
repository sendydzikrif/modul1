import java.awt.Color;
public class Lion {
	public String nama;
	public int usia;
	public double bb;
	public boolean statusJinak;
	public String habitat;

	public void cetakInformasi () {
		System.out.println("Lion bernama : " +nama);
		System.out.println("Usia : " +usia);
		System.out.println("Berat Badan : " +bb);
		System.out.println("Jinak ? : " +apakahJinak());
		System.out.println("Habitat di : " +habitat);
	}

	public void AlamLiar (String h) {
		habitat = h;
		statusJinak = false;
	}

	public boolean apakahJinak () {
		return statusJinak;
	}

	public void Penangkaran () {
		habitat = " ";
		statusJinak = true;
	}
}