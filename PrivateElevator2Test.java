public class PrivateElevator2Test {
	public static void main(String args[]){
		PrivateElevator2 privEle = new PrivateElevator2();
		privEle.buka();
		privEle.tutup();
		privEle.turun();
		privEle.naik();
		privEle.naik();
		privEle.buka();
		privEle.tutup();
		privEle.turun();
		privEle.buka();
		privEle.turun();
		privEle.tutup();
		privEle.turun();
		privEle.turun();

		int lantai = privEle.getLantai();

		if(lantai != 5 && !privEle.getStatusPintu()) {
			privEle.setLantai(5);
		}

		privEle.setLantai(10);
		privEle.buka();
	}
}
