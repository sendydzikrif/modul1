public class Square extends Shape {
	public String color;

	public String getColor(){
		return color;
	}

	public void setColor(String color){
		this.color = color;
	}


	public static void main (String[] args) {
		Square box = new Square();
		
		System.out.println(" Nama : ");
		box.setName("Box Mie");
		System.out.println(box.getName());

		System.out.println("------------------------------");

		System.out.println(" Area : ");
		box.setArea("500cm");
		System.out.println(box.getArea());

		System.out.println("------------------------------");

		System.out.println(" Warna : ");
		box.setColor("Brown");
		System.out.println(box.getColor());

		System.out.println("------------------------------");
		
	}
	
}