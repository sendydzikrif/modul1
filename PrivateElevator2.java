public class PrivateElevator2 {
 public boolean bukaPintu = false;
 public int lantaiSkrg = 1;
 public int berat = 0;

 public final int Kapasitas = 1000;
 public final int Lantai_Atas = 5;
 public final int Lantai_Bawah = 1;

	public void buka(){
		bukaPintu = true;
	}

	public void tutup(){
		hitungKapasitas();
		if(berat<= Kapasitas) {
			bukaPintu=false;
		}else {
			System.out.println("Elevator kelebihan beban");
			System.out.println("Pintu akan tetap terbuka sampai seseorang keluar");
		}
	}

	//pada dunia nyata, elevator menggunakan sensor berat untuk memeriksa
	//beban, tetapi agar lebih sederhana, kami menggunakan bilangan acak untuk berat

	public void hitungKapasitas(){
		berat=(int)(Math.random()*1500);
		System.out.println("Berat : "+berat);
	}

	public void naik(){
		if(!bukaPintu){
			if(lantaiSkrg< Lantai_Atas ){
				lantaiSkrg++;
				System.out.println(lantaiSkrg);
			} else {
				System.out.println("Sudah mencapai lantai atas");
			}
		} else {
			System.out.println("Pintu masih terbuka");
		}
	}

	public void turun(){
		if(!bukaPintu) {
			if(lantaiSkrg > Lantai_Bawah) {
				lantaiSkrg--;
				System.out.println(lantaiSkrg);
			} else {
				System.out.println("Sudah mencapai lantai bawah");
			}
		} else {
			System.out.println("Pintu masih terbuka");
		}
	}

	public void setLantai(int tujuan) {
		if((tujuan >= Lantai_Bawah) && (tujuan <= Lantai_Atas)) {
			while (lantaiSkrg != tujuan) {
				if(lantaiSkrg < tujuan) {
					naik();
				} else {
					turun();
				}
			}
		} else {
			System.out.println("Lantai Salah");
		}
	}

	public int getLantai(){
		return lantaiSkrg;
	}

	public boolean getStatusPintu() {
		return bukaPintu;
	}


}
